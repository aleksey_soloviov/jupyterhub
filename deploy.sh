#!/bin/bash -x

HOST_ROOT_FOLDER='/opt/jupyter/jupyterhub'

if [ ! -d $HOST_ROOT_FOLDER ]; then
    mkdir $HOST_ROOT_FOLDER
fi

/bin/cp -rf ./jupyterhub_config.py $HOST_ROOT_FOLDER/

docker stack deploy -c docker-compose.yml jupyterhub
